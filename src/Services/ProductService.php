<?php
namespace App\Services;

use App\Entity\Product;
use App\Model\ProductRepositoryInterface;
use App\Dto\ProductDTO;

final class ProductService
{
    /**
     * @var ProductRepository
     */
    private $productRepo;

    /**
     * @var ProductAssembler
     */
    private $productAssembler;


    /**
     * ProductService constructor
     * @param ProductRepositoryInterface $productRepo
     * @param ProductAssembler $productAssembler
     */
    public function __construct(
        ProductRepositoryInterface $productRepo,
        ProductAssembler $productAssembler
    ) {
        $this->productRepo = $productRepo;
        $this->productAssembler = $productAssembler;
    }


    /**
     * @param ProductDTO $DTO
     * @return Product
     */
    public function addProduct(ProductDTO $DTO) : Product
    {
        $Product = $this->productAssembler->createProduct($DTO);
        $this->productRepo->save($Product);

        return $Product;
    }


    public function updateProduct(Product $Product)
    {
        $this->productRepo->save($Product);
    }


    public function getAllProducts()
    {
        return $this->productRepo->findAll();
    }
}
