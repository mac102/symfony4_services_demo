<?php
namespace App\Services;

use App\Entity\Product;
use App\Dto\ProductDTO;

class ProductAssembler
{
    private $ProductDTO;
    private $Product;

    public function readDTO(ProductDTO $DTO, ?Product $Product = null) : Product  // pytajnik oznacza że może byc null
    {
        if ($Product == null) {
          $Product = new Product();
        }
        $Product->setName($DTO->getName());
        $Product->setDescription($DTO->getDescription());
        return $Product;
    }


    public function updateProduct(Product $Product, ProductDTO $DTO)
    {
        return $this->readDTO($DTO, $Product);
    }


    public function createProduct(ProductDTO $DTO)
    {
        return $this->readDTO($DTO);
    }


    public function writeDTO(Product $Product) : ProductDTO
    {
        return new ProductDTO(
            $Product->getName(),
            $Product->getDescription()
        );
    }
}
