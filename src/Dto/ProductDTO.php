<?php
namespace App\Dto;

use JMS\Serializer\Annotation\Type;

//DTO - data transfer object
final class ProductDTO
{
    /**
     * @Type("string")
     */
    private $name;

    /**
     * @Type("string")
     */
    private $description;

    public function __construct(string $name, string $description)
    {
        $this->name = $name;
        $this->description = $description;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }
}
