<?php
namespace App\Model;
use App\Entity\Product;

interface ProductRepositoryInterface
{
    public function find(int $productId): Product;

    public function findAll(): ?array;

    public function findOneByTitle(string $title): Product;

    public function save(Product $product): void;
}
