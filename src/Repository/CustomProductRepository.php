<?php

namespace App\Repository;

use App\Entity\Product;
use App\Model\ProductRepositoryInterface;
//use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
//use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
//class ProductRepository extends ServiceEntityRepository
class CustomProductRepository implements ProductRepositoryInterface
{
    private $entityManager;

    private $objectRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        //echo $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Product::class);
    }

    public function find(int $productId): Product
    {
        return $this->objectRepository->find($productId);
    }

    public function findAll(): ?array
    {
        return $this->objectRepository->findAll();
        //return array();
    }

    public function findOneByTitle(string $title): Product
    {
        $product = $this->objectRepository
            ->findOneBy(['title' => $title]);
        return $product;
    }

    public function save(Product $product): void
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }
}
