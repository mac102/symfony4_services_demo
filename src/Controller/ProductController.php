<?php
namespace App\Controller;

use App\Dto\ProductDTO;
use App\Entity\Product;
use App\Services\ProductService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends FOSRestController
{
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * Product controller constructor
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }


    /**
     * Create product
     * @Rest\Post("/products")
     * @ParamConverter("productDTO", converter="fos_rest.request_body")
     *
     * @param ProductDTO $productDTO
     */
     public function create(ProductDTO $productDTO)
     {
        $Product = $this->productService->addProduct($productDTO);

        return new Response(
            'Product added',
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );
     }


    /**
     */
    /*public function edit($productId): Response
    {

    } */


    /**
     * Pobiera kolekcję produktów
     * @Rest\Get("/products")
     */
    public function getProductsAction(): Response
    {
        $products = $this->productService->getAllProducts();
        $res = '';
        foreach($products as $Product) {
          $res .= $Product->getName().PHP_EOL;
        }
        if ($res == '') {
          $res = 'No product found :(.';
        }
        return new Response(
            $res,
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );
    }
}
